package co.com.sofka.automation.models;

import co.com.sofka.automation.models.dto.DTOAccount;
import com.github.javafaker.Faker;

import java.security.SecureRandom;
import java.util.Locale;

public class DataAccount {

    private DataAccount() {
    }

    private static Faker faker = Faker.instance(new Locale("es", "CO"), new SecureRandom());

    public static DTOAccount createdDataAccount() {
        return DTOAccount.builder()
                .email(faker.internet().emailAddress().replace(" ", ""))
                .firstName(faker.name().firstName())
                .lastName(faker.name().lastName())
                .password(faker.internet().password())
                .birthDay(String.valueOf(faker.number().numberBetween(1, 28)))
                .monthDay(String.valueOf(faker.number().numberBetween(1, 12)))
                .yearDay(String.valueOf(faker.number().numberBetween(1960, 2021)))
                .company(faker.company().name())
                .address(faker.address().fullAddress())
                .city(faker.address().city())
                .idState(String.valueOf(faker.number().numberBetween(1, 50)))
                .postcode(String.valueOf(faker.number().numberBetween(10000, 99999)))
                .phone(String.valueOf(faker.number().numberBetween(1111111, 9999999)))
                .phoneMobile("3".concat(String.valueOf(faker.number().numberBetween(100000000, 900000000))))
                .build();
    }
}
