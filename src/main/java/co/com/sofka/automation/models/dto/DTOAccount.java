package co.com.sofka.automation.models.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DTOAccount {

    private String email;
    private String firstName;
    private String lastName;
    private String password;
    private String birthDay;
    private String monthDay;
    private String yearDay;
    private String company;
    private String address;
    private String city;
    private String idState;
    private String postcode;
    private String phone;
    private String phoneMobile;
}
