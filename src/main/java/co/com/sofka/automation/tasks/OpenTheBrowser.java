package co.com.sofka.automation.tasks;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class OpenTheBrowser implements Task {

    PageObject pageObject;

    public OpenTheBrowser(PageObject pageObject){
        this.pageObject = pageObject;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Open.browserOn(pageObject));
    }

    public static OpenTheBrowser on (PageObject pageObject){
        return instrumented(OpenTheBrowser.class,pageObject);
    }
}
