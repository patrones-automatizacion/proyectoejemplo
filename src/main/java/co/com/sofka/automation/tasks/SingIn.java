package co.com.sofka.automation.tasks;

import co.com.sofka.automation.models.dto.DTOAccount;
import co.com.sofka.automation.ui.EcommerceHomePage;
import co.com.sofka.automation.ui.EcommerceLoginPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SingIn implements Task {

    DTOAccount account;

    public SingIn(DTOAccount account) {
        this.account = account;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(account.getEmail()).into(EcommerceLoginPage.EMAIL_INPUT),
                Enter.theValue(account.getPassword()).into(EcommerceLoginPage.PASSWORD_INPUT),
                Click.on(EcommerceLoginPage.SING_IN_BUTTON),
                WaitUntil.the(EcommerceHomePage.BODY_CONTAINER, isVisible())
        );
    }

    public static SingIn go(DTOAccount account) {
        return instrumented(SingIn.class, account);
    }
}
