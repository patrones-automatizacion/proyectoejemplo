package co.com.sofka.automation.tasks;

import co.com.sofka.automation.ui.EcommerceCreatedAccountPage;
import co.com.sofka.automation.ui.EcommerceHomePage;
import co.com.sofka.automation.ui.EcommerceLoginPage;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SingUp implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(EcommerceHomePage.SING_OUT),
                WaitUntil.the(EcommerceLoginPage.EMAIL_INPUT, isVisible())
        );
    }

    public static SingUp go(){
        return instrumented(SingUp.class);
    }
}
