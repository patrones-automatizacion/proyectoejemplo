package co.com.sofka.automation.tasks;

import co.com.sofka.automation.ui.EcommerceIndexPage;
import co.com.sofka.automation.ui.EcommerceLoginPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class CreatedAccount implements Task {

    String email;

    public CreatedAccount(String email) {
        this.email = email;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(EcommerceIndexPage.LOGIN_BUTTON),
                Enter.theValue(email).into(EcommerceLoginPage.EMAIL_CREATED_INPUT),
                Click.on(EcommerceLoginPage.CREATED_ACCOUNT_BUTTON)
        );
    }

    public static CreatedAccount go(String email) {
        return instrumented(CreatedAccount.class, email);
    }
}
