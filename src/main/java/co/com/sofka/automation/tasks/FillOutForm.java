package co.com.sofka.automation.tasks;

import co.com.sofka.automation.models.dto.DTOAccount;
import co.com.sofka.automation.ui.EcommerceCreatedAccountPage;
import co.com.sofka.automation.ui.EcommerceHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class FillOutForm implements Task {

    DTOAccount account;

    public FillOutForm(DTOAccount account) {
        this.account = account;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(account.getFirstName()).into(EcommerceCreatedAccountPage.FIRST_NAME_INPUT),
                Enter.theValue(account.getLastName()).into(EcommerceCreatedAccountPage.LAST_NAME_INPUT),
                Enter.theValue(account.getPassword()).into(EcommerceCreatedAccountPage.PASSWORD_INPUT),
                SelectFromOptions.byValue(account.getBirthDay()).from(EcommerceCreatedAccountPage.DAY_BIRTH_SELECT),
                SelectFromOptions.byValue(account.getMonthDay()).from(EcommerceCreatedAccountPage.MONTH_BIRTH_SELECT),
                SelectFromOptions.byValue(account.getYearDay()).from(EcommerceCreatedAccountPage.YEAR_BIRTH_SELECT),
                Enter.theValue(account.getCompany()).into(EcommerceCreatedAccountPage.COMPANY_INPUT),
                Enter.theValue(account.getAddress()).into(EcommerceCreatedAccountPage.ADDRESS_INPUT),
                Enter.theValue(account.getCity()).into(EcommerceCreatedAccountPage.CITY_INPUT),
                SelectFromOptions.byValue(account.getIdState()).from(EcommerceCreatedAccountPage.STATE_SELECT),
                Enter.theValue(account.getPostcode()).into(EcommerceCreatedAccountPage.POSTAL_CODE_INPUT),
                Enter.theValue(account.getPhone()).into(EcommerceCreatedAccountPage.PHONE_NUMBER_INPUT),
                Enter.theValue(account.getPhoneMobile()).into(EcommerceCreatedAccountPage.PHONE_MOBILE_INPUT),
                Click.on(EcommerceCreatedAccountPage.REGISTER_BUTTON),
                WaitUntil.the(EcommerceHomePage.BODY_CONTAINER, isVisible())
        );
    }


    public static FillOutForm proceed(DTOAccount account) {
        return instrumented(FillOutForm.class, account);
    }


}
