package co.com.sofka.automation.ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class EcommerceCreatedAccountPage extends PageObject {

    public static final Target FIRST_NAME_INPUT = Target.the("Input primer nombre")
            .located(By.id("customer_firstname"));

    public static final Target LAST_NAME_INPUT = Target.the("Input primer apellido")
            .located(By.id("customer_lastname"));

    public static final Target PASSWORD_INPUT = Target.the("Input contraseña")
            .located(By.id("passwd"));

    public static final Target DAY_BIRTH_SELECT = Target.the("Lista día de nacimiento")
            .located(By.id("days"));

    public static final Target MONTH_BIRTH_SELECT = Target.the("Lista mes de nacimiento")
            .located(By.id("months"));

    public static final Target YEAR_BIRTH_SELECT = Target.the("Lista año de nacimiento")
            .located(By.id("years"));

    public static final Target COMPANY_INPUT = Target.the("Input nombre compañia")
            .located(By.id("company"));

    public static final Target ADDRESS_INPUT = Target.the("Input direccion")
            .located(By.id("address1"));

    public static final Target CITY_INPUT = Target.the("Input ciudad")
            .located(By.id("city"));

    public static final Target STATE_SELECT = Target.the("Lista estado")
            .located(By.id("id_state"));

    public static final Target POSTAL_CODE_INPUT = Target.the("Input código postal")
            .located(By.id("postcode"));

    public static final Target PHONE_NUMBER_INPUT = Target.the("Input número telefonico")
            .located(By.id("phone"));

    public static final Target PHONE_MOBILE_INPUT = Target.the("Input número celular")
            .located(By.id("phone_mobile"));

    public static final Target REGISTER_BUTTON = Target.the("Botón registrar")
            .located(By.id("submitAccount"));

}
