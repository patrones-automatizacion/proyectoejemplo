package co.com.sofka.automation.ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;


@DefaultUrl("http://automationpractice.com/index.php")
public class EcommerceIndexPage extends PageObject {

    public static final Target LOGIN_BUTTON = Target.the("Botón Login")
            .locatedBy("//*[@class='login']");
}
