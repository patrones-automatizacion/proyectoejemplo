package co.com.sofka.automation.ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class EcommerceLoginPage extends PageObject {

    public static final Target EMAIL_CREATED_INPUT = Target.the("Input crear email")
            .located(By.id("email_create"));
    public static final Target CREATED_ACCOUNT_BUTTON = Target.the("Botón crear cuenta")
            .located(By.id("SubmitCreate"));
    public static final Target EMAIL_INPUT = Target.the("Input email")
            .located(By.id("email"));
    public static final Target PASSWORD_INPUT = Target.the("Input password")
            .located(By.id("passwd"));
    public static final Target SING_IN_BUTTON = Target.the("Botón iniciar sesión")
            .located(By.id("SubmitLogin"));

}
