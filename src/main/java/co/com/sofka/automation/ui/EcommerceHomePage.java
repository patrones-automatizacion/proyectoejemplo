package co.com.sofka.automation.ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class EcommerceHomePage extends PageObject {

    public static final Target BODY_CONTAINER = Target.the("Contenedor principal")
            .located(By.id("my-account"));

    public static final Target USER_NAME_LABEL = Target.the("Label nombre usuario")
            .locatedBy("//*[@class='account']/span");

    public static final Target SING_OUT = Target.the("Botón cerrar sesión")
            .locatedBy("//*[@class='logout']");

}
