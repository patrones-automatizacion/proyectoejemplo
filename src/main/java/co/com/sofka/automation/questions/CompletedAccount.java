package co.com.sofka.automation.questions;

import co.com.sofka.automation.ui.EcommerceHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class CompletedAccount implements Question<String> {


    @Override
    public String answeredBy(Actor actor) {
        return EcommerceHomePage.USER_NAME_LABEL.resolveFor(actor).getText();
    }

    public static CompletedAccount ok() {
        return new CompletedAccount();
    }
}
