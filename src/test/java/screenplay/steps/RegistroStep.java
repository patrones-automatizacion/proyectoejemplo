package screenplay.steps;

import co.com.sofka.automation.models.DataAccount;
import co.com.sofka.automation.models.dto.DTOAccount;
import co.com.sofka.automation.questions.CompletedAccount;
import co.com.sofka.automation.tasks.*;
import co.com.sofka.automation.ui.EcommerceIndexPage;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import io.github.bonigarcia.wdm.WebDriverManager;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import org.hamcrest.Matchers;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.is;

public class RegistroStep {

    private Actor actor;
    private DTOAccount account;
    private EcommerceIndexPage indexPage;

    @Before
    public void setup() {
        WebDriverManager.chromedriver().setup();
        OnStage.setTheStage(new OnlineCast());
        actor = OnStage.theActorCalled("UsuarioNuevo");
        account = DataAccount.createdDataAccount();
    }


    @Dado("^un usuario sin correo electronico registrado en el ecommerce$")
    public void unUsuarioSinCorreoElectronicoRegistradoEnElEcommerce() {
        actor.wasAbleTo(OpenTheBrowser.on(indexPage));
        actor.wasAbleTo(CreatedAccount.go(account.getEmail()));
    }

    @Cuando("^diligencia el formulario de registro correctamente$")
    public void diligenciaElFormularioDeRegistroCorrectamente() {
        actor.attemptsTo(FillOutForm.proceed(account));
    }

    @Entonces("^creara su usuario exitosamente en el ecommerce$")
    public void crearaSuUsuarioExitosamenteEnElEcommerce() {
        actor.should(seeThat(CompletedAccount.ok(), is(Matchers.containsString(account.getFirstName()))));
        actor.should(seeThat(CompletedAccount.ok(), is(Matchers.containsString(account.getLastName()))));
        actor.attemptsTo(SingUp.go());
        actor.attemptsTo(SingIn.go(account));
        actor.attemptsTo(SingUp.go());
    }
}
