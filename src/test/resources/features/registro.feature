#language: es

Característica: Registro de usuario nuevo en el sitio automation practice

  Escenario: : Registro exitoso en el ecommerce
    Dado un usuario sin correo electronico registrado en el ecommerce
    Cuando diligencia el formulario de registro correctamente
    Entonces creara su usuario exitosamente en el ecommerce
